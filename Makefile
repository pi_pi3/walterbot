SRC:=build.amp src/main.amp src/span.amp src/sexpr.amp src/lex.amp src/parse.amp src/interp.amp

.PHONY: run

all: target/walterbot.so

run: target/walterbot.so
	./run.sh

target/walterbot.o: $(SRC)
	etc build

target/walterbot.so: target/walterbot.o
	clang ./target/walterbot.o tmi_cxx/build/Release/tmi_cxx.node -o target/walterbot.so -lc -lm -fPIC -shared
