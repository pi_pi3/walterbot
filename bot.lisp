(define name "WalterBot")
(define project "WalterBot")
(define logfile "/tmp/chat.log")

(define printf-counter 1)
(define commands
    '(("project" . '(say "Today we are working on ~(->string project)."))
      ("bot" . '(say "I am ~(->string name), beep boop."))
      ("quote" . '(say (nth quotes (random 0 (len quotes)))))
      ("printf" . '(begin 
                    (inc printf-counter)
                    (say "printf debugging btw ~(->string printf-counter)")))
      ("eval" . '(say "~(with-env () (eval (parse ~(->string @0))))"))
      ("rm" . '(say "rm -rf --no-preserve-root /"))
      ("rust" . '(say "no rust november"))
      ("help" . '(say "!project !bot !quote !help !addcmd"))))
(define quotes '("If you used linux, you wouldn't have any problems. - pie, 2018"))
