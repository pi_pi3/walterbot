with __lang::macros;
with __lang::macros::info;
with __lang::macros::pkg;
with __lang::macros::build;

macro package: () -> unit = {
    pkg::name("walterbot");
    pkg::version("0.1.0");
    pkg::authors(["Szymon Walter <waltersz@protonmail.com>"]);
    pkg::description("A twitch chatbot");
    pkg::kind("lib");
    pkg::repository("https://gitlab.com/pi_pi3/walterbot");
}

macro build: () -> unit = {
    info("include C dir: /usr/lib/clang/8.0.1/include");
    build::add_cpath("/usr/lib/clang/8.0.1/include");
    info("include C dir: ./tmi_cxx/include");
    build::add_cpath("./tmi_cxx/include");

    info("include & dir: src");
    build::add_path("src");

    info("option: --relocation=pie");
    build::set_opt("relocation", "pie");

    info("option: --emit=obj");
    build::set_opt("emit", "obj");

    info("option: --define _GNU_SOURCE=1");
    build::set_opt("define", "_GNU_SOURCE=1");

    info("option: --package=walterbot");
    build::set_opt("package", "walterbot");

    if macros::ctx.release {
        info("option: -O3");
        build::set_opt("O", "3");
    } else {
        info("flag: --debuginfo");
        build::set_flag("debuginfo", true);
    }

    info("add & file: main.amp");
    build::add_file("main.amp");
    info("add & file: span.amp");
    build::add_file("span.amp");
    info("add & file: sexpr.amp");
    build::add_file("sexpr.amp");
    info("add & file: lex.amp");
    build::add_file("lex.amp");
    info("add & file: parse.amp");
    build::add_file("parse.amp");
    info("add & file: interp.amp");
    build::add_file("interp.amp");

    // libstd
    info("add & file: std/collections/opt.amp");
    build::add_file("std/collections/opt.amp");
    info("add & file: std/collections/res.amp");
    build::add_file("std/collections/res.amp");
    info("add & file: std/collections/array.amp");
    build::add_file("std/collections/array.amp");
    info("add & file: std/algo/hash.amp");
    build::add_file("std/algo/hash.amp");
    info("add & file: std/box.amp");
    build::add_file("std/box.amp");
    info("add & file: std/io.amp");
    build::add_file("std/io.amp");
    info("add & file: std/string.amp");
    build::add_file("std/string.amp");
    info("add & file: std/dstring.amp");
    build::add_file("std/dstring.amp");
    info("add & file: std/ref.amp");
    build::add_file("std/ref.amp");
    info("add & file: std/num.amp");
    build::add_file("std/num.amp");

    info("set output: walterbot.o");
    build::set_output("walterbot.o");
}
