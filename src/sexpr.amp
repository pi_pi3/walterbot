mod walterbot::sexpr;

use std::string;
use std::box;
use std::dstring;
use std::collections::array;
use std::collections::opt;
use walterbot::span;

with std::string::*;
with std::box::*;
with std::dstring::*;
with std::collections::array::*;
with std::collections::opt::*;
with walterbot::span::*;

define Sexpr = tagged(
    span: Option(T: Span),
    // expression
    Nil(),
    True(),
    Int(int: s64),
    Float(flt: float64),
    Atom(string: String),
    Str(string: String),
    Pair(
        lt: Box(T: Sexpr),
        rt: Box(T: Sexpr),
    ),
    // builtins
    Lambda(
        params: Array(T: String),
        body: Box(T: Sexpr),
    ),
    Quote(inner: Box(T: Sexpr)),
    Quasiquote(inner: Box(T: Sexpr)),
    Interp(inner: Box(T: Sexpr)),
    Error(msg: String),
);

fn nil: () -> Box(T: Sexpr) = {
    let expr = Sexpr::Nil(span: Option::None(T: Span));
    expr.tag = 0;
    return boxed(expr);
}

fn t: () -> Box(T: Sexpr) = {
    let expr = Sexpr::True(span: Option::None(T: Span));
    expr.tag = 1;
    return boxed(expr);
}

fn number: (number: s64) -> Box(T: Sexpr) = {
    let expr = Sexpr::Int(span: Option::None(T: Span), int: number);
    return boxed(expr);
}

fn number: (number: float64) -> Box(T: Sexpr) = {
    let expr = Sexpr::Float(span: Option::None(T: Span), flt: number);
    return boxed(expr);
}

fn lstring: (string: str) -> Box(T: Sexpr) = {
    let expr = Sexpr::Str(span: Option::None(T: Span), string: to_string(string));
    return boxed(expr);
}

fn atom: (string: str) -> Box(T: Sexpr) = {
    let expr = Sexpr::Atom(span: Option::None(T: Span), string: to_string(string));
    return boxed(expr);
}

fn pair: (a: Box(T: Sexpr), b: Box(T: Sexpr)) -> Box(T: Sexpr) = {
    let expr = Sexpr::Pair(span: Option::None(T: Span), lt: a, rt: b);
    return boxed(expr);
}

fn list_of: (array: ..Box(T: Sexpr)) -> Box(T: Sexpr) = {
    let list = nil();
    for let j = array.len; j > 0; j-- {
        let elem = array[j - 1];
        list = boxed(Sexpr::Pair(span: Option::None(T: Span), lt: elem, rt: list));
    }
    return list;
}

fn error: (msg: str) -> Box(T: Sexpr) = {
    let expr = Sexpr::Error(span: Option::None(T: Span), msg: to_string(msg));
    return boxed(expr);
}

fn del: (expr: Sexpr) -> unit = {
    match &expr {
        Sexpr::Atom(string) => del(*string);
        Sexpr::Str(string) => del(*string);
        Sexpr::Pair(lt, rt) => {
            del(*lt);
            del(*rt);
        }
        Sexpr::Lambda(params, body) => {
            del(*params);
            del(*body);
        }
        Sexpr::Quote(inner) => del(*inner);
        Sexpr::Quasiquote(inner) => del(*inner);
        Sexpr::Interp(inner) => del(*inner);
    }
}

fn clone: (expr: Sexpr) -> Sexpr = {
    match &expr {
        Sexpr::Atom(string) => return Sexpr::Atom(string: clone(*string));
        Sexpr::Str(string) => return Sexpr::Str(string: clone(*string));
        Sexpr::Pair(lt, rt) => return Sexpr::Pair(lt: clone(*lt), rt: clone(*rt));
        Sexpr::Lambda(params, body) => return Sexpr::Lambda(parms: clone(*params), body: clone(*body));
        Sexpr::Quote(inner) => return Sexpr::Quote(inner: clone(*inner));
        Sexpr::Quasiquote(inner) => return Sexpr::Quasiquote(inner: clone(*inner));
        Sexpr::Interp(inner) => return Sexpr::Interp(inner: clone(*inner));
        else => return expr;
    }
}

fn is_nil: (expr: Ref(T: Sexpr)) -> bool = {
    match expr.ptr {
        Sexpr::Nil() => return true;
        else => return false;
    }
}

fn is_error: (expr: Ref(T: Sexpr)) -> bool = {
    match expr.ptr {
        Sexpr::Error() => return true;
        else => return false;
    }
}

fn is_list: (expr: Ref(T: Sexpr)) -> bool = {
    match expr.ptr {
        Sexpr::Nil() => return true;
        Sexpr::Pair(rt) => return is_list(as_ref(*rt));
        else => return false;
    }
}

fn head_or_undef: (expr: Ref(T: Sexpr)) -> Ref(T: Sexpr) = {
    match expr.ptr {
        Sexpr::Pair(lt) => return as_ref(*lt);
        else => return result;
    }
}

fn tail_or_undef: (expr: Ref(T: Sexpr)) -> Ref(T: Sexpr) = {
    match expr.ptr {
        Sexpr::Pair(rt) => return as_ref(*rt);
        else => return result;
    }
}

fn take_nth: (list: Box(T: Sexpr), n: uint) -> Option(T: Box(T: Sexpr)) = {
    if n == 0 {
        match list.ptr {
            Sexpr::Pair(lt, rt) => {
                // take out head
                result = Option::Some(some: *lt);
                // concat next element with previous element
                *list.ptr = *rt.ptr;
                // set next element to nil
                rt.ptr.tag = 0;
                // deallocate memory
                del(*rt);
                return result;
            }
            else => return Option::None(T: Box(T: Sexpr));
        }
    }
    match list.ptr {
        Sexpr::Pair(rt) => return take_nth(*rt, n - 1);
        else => return Option::None(T: Box(T: Sexpr));
    }
}

fn nth: (list: Ref(T: Sexpr), n: uint) -> Option(T: Ref(T: Sexpr)) = {
    if n == 0 {
        match list.ptr {
            Sexpr::Pair(lt) => return Option::Some(some: as_ref(*lt));
            else => return Option::None(T: Ref(T: Sexpr));
        }
    }
    match list.ptr {
        Sexpr::Pair(rt) => return nth(as_ref(*rt), n - 1);
        else => return Option::None(T: Ref(T: Sexpr));
    }
}

fn len: (list: Ref(T: Sexpr)) -> uint = {
    fn inner: (list: Ref(T: Sexpr), acc: uint) -> uint = {
        match list.ptr {
            Sexpr::Pair(rt) => return inner(as_ref(*rt), acc + 1);
            else => return acc;
        }
    }
    return inner(list, 0);
}

fn __eq__: (lhs: Sexpr, rhs: Sexpr) -> bool = {
    // TODO
    match &lhs {
        Sexpr::Atom(string: lhs) => {
            match &rhs {
                Sexpr::Atom(string: rhs) => {
                    return as_str(*lhs) == as_str(*rhs);
                }
                // FIXME
                else => {}
            }
        }
        // FIXME
        else => {}
    }
    return false;
}

fn push: (list: Box(T: Sexpr), value: Box(T: Sexpr)) -> Box(T: Sexpr) = {
    let list = Sexpr::Pair(lt: value, rt: list);
    return boxed(list);
}

fn pop: (list: Box(T: Sexpr)) -> Box(T: Sexpr) = {
    match list.ptr {
        Sexpr::Pair(lt, rt) => {
            result = *rt;

            *rt = nil();
            del(list);

            return result;
        }
        // FIXME
        else => {}
    }
    return list;
}

fn enter: (list: Box(T: Sexpr)) -> Box(T: Sexpr) = {
    return push(list, nil());
}

fn leave: (list: Box(T: Sexpr)) -> Box(T: Sexpr) = {
    return pop(list);
}

fn set: (list: Box(T: Sexpr), key: Box(T: Sexpr), value: Box(T: Sexpr)) -> Box(T: Sexpr) = {
    let pair = Sexpr::Pair(lt: key, rt: value);
    return push(list, boxed(pair));
}

fn get: (list: Ref(T: Sexpr), key: Ref(T: Sexpr)) -> Option(T: Ref(T: Sexpr)) = {
    match list.ptr {
        Sexpr::Pair(lt, rt) => {
            match lt.ptr {
                Sexpr::Pair(lt, rt) => {
                    if *lt.ptr == *key.ptr {
                        return Option::Some(some: as_ref(*rt));
                    }
                }
                // FIXME
                else => {}
            }
            return get(as_ref(*rt), key);
        }
        // FIXME
        else => {}
    }
    return Option::None(T: Ref(T: Sexpr));
}

define ListIter = struct(list: Ref(T: Sexpr));

fn list_iter: (expr: Ref(T: Sexpr)) -> ListIter = ListIter(list: expr);

with __lang::macros::Node;
with __lang::macros::Typed;
with __lang::macros::quasiquote;

macro __iter__: (it: Node, list: Typed(T: ListIter), body: Node) -> Node = {
    return quasiquote! {
        match ~(list.node).list.ptr {
            Sexpr::Nil() => {}
            Sexpr::Pair(lt, rt) => {
                let ~(it) = as_ref(*lt);
                let tail = as_ref(*rt);
                while true {
                    ~(body);
                    match tail.ptr {
                        Sexpr::Nil() => break;
                        Sexpr::Pair(lt, rt) => {
                            ~(it) = as_ref(*lt);
                            tail = as_ref(*rt);
                        }
                    }
                }
            }
        }
    }
}

fn to_string: (expr: Ref(T: Sexpr)) -> String = {
    match expr.ptr {
        Sexpr::Nil() => return to_string("#nil");
        Sexpr::True() => return to_string("#t");
        Sexpr::Int(int) => {
            new(&result, "");
            grow_to(&result, 1024);
            set_len(&result, 1024);
            let len = snprintf(ptr(result), 1024, "%ld".ptr, *int);
            set_len(&result, len);
            return result;
        }
        Sexpr::Float(flt) => {
            new(&result, "");
            grow_to(&result, 1024);
            set_len(&result, 1024);
            let len = snprintf(ptr(result), 1024, "%f".ptr, *flt);
            set_len(&result, len);
            return result;
        }
        Sexpr::Atom(string) => {
            return to_string(as_str(*string));
        }
        Sexpr::Str(string) => {
            return to_string(as_str(*string));
        }
        Sexpr::Pair(lt, rt) => {
            if is_list(expr) {
                new(&result, "");
                grow_to(&result, 1024);
                set_len(&result, 1024);
                let n = 0;
                n += snprintf(&ptr(result)[n], 1024 - n, "(".ptr);
                let i = 0;
                foreach elem in list_iter(expr) {
                    if i > 0 {
                        n += snprintf(&ptr(result)[n], 1024 - n, " ".ptr);
                    }
                    let elem_str = repr(elem);
                    defer del(elem_str);
                    n += snprintf(&ptr(result)[n], 1024 - n, "%.*s".ptr, len(elem_str), ptr(elem_str));
                    ++i;
                }
                n += snprintf(&ptr(result)[n], 1024 - n, ")".ptr);
                set_len(&result, n);
                return result;
            } else {
                let lhs = repr(as_ref(*lt));
                defer del(lhs);
                let rhs = repr(as_ref(*rt));
                defer del(rhs);
                new(&result, "");
                grow_to(&result, 1024);
                set_len(&result, 1024);
                let len = snprintf(ptr(result), 1024, "(%.*s . %.*s)".ptr, len(lhs), ptr(lhs), len(rhs), ptr(rhs));
                set_len(&result, len);
                return result;
            }
        }
        Sexpr::Error(msg) => {
            new(&result, "");
            grow_to(&result, 1024);
            set_len(&result, 1024);
            let len = snprintf(ptr(result), 1024, "error: %.*s".ptr, len(*msg), ptr(*msg));
            set_len(&result, len);
            return result;
        }
        else => return to_string("<>");
    }
}

fn repr: (expr: Ref(T: Sexpr)) -> String = {
    match expr.ptr {
        Sexpr::Nil() => return to_string("()");
        Sexpr::True() => return to_string("#t");
        Sexpr::Int(int) => {
            new(&result, "");
            grow_to(&result, 1024);
            set_len(&result, 1024);
            let len = snprintf(ptr(result), 1024, "%ld".ptr, *int);
            set_len(&result, len);
            return result;
        }
        Sexpr::Float(flt) => {
            new(&result, "");
            grow_to(&result, 1024);
            set_len(&result, 1024);
            let len = snprintf(ptr(result), 1024, "%f".ptr, *flt);
            set_len(&result, len);
            return result;
        }
        Sexpr::Atom(string) => {
            return to_string(as_str(*string));
        }
        Sexpr::Str(string) => {
            new(&result, "");
            grow_to(&result, 1024);
            set_len(&result, 1024);
            let len = snprintf(ptr(result), 1024, "\"%.*s\"".ptr, len(*string), ptr(*string));
            set_len(&result, len);
            return result;
        }
        Sexpr::Pair(lt, rt) => {
            if is_list(expr) {
                new(&result, "");
                grow_to(&result, 1024);
                set_len(&result, 1024);
                let n = 0;
                n += snprintf(&ptr(result)[n], 1024 - n, "(".ptr);
                let i = 0;
                foreach elem in list_iter(expr) {
                    if i > 0 {
                        n += snprintf(&ptr(result)[n], 1024 - n, " ".ptr);
                    }
                    let elem_str = repr(elem);
                    defer del(elem_str);
                    n += snprintf(&ptr(result)[n], 1024 - n, "%.*s".ptr, len(elem_str), ptr(elem_str));
                    ++i;
                }
                n += snprintf(&ptr(result)[n], 1024 - n, ")".ptr);
                set_len(&result, n);
                return result;
            } else {
                let lhs = repr(as_ref(*lt));
                defer del(lhs);
                let rhs = repr(as_ref(*rt));
                defer del(rhs);
                new(&result, "");
                grow_to(&result, 1024);
                set_len(&result, 1024);
                let len = snprintf(ptr(result), 1024, "(%.*s . %.*s)".ptr, len(lhs), ptr(lhs), len(rhs), ptr(rhs));
                set_len(&result, len);
                return result;
            }
        }
        Sexpr::Error(msg) => {
            new(&result, "");
            grow_to(&result, 1024);
            set_len(&result, 1024);
            let len = snprintf(ptr(result), 1024, "error: %.*s".ptr, len(*msg), ptr(*msg));
            set_len(&result, len);
            return result;
        }
        else => return to_string("<>");
    }
}

fn debug: (expr: &Sexpr, file: &FILE) -> uint = {
    match expr {
        Sexpr::Nil() => return write(&"Sexpr::Nil", file);
        Sexpr::True() => return write(&"Sexpr::True", file);
        Sexpr::Int(int) => {
            let n = 0;
            n += write(&"Sexpr::Int(", file);
            n += debug(int, file);
            n += write(&")", file);
            return n;
        }
        Sexpr::Float(flt) => {
            let n = 0;
            n += write(&"Sexpr::Float(", file);
            n += debug(flt, file);
            n += write(&")", file);
            return n;
        }
        Sexpr::Atom(string) => {
            let n = 0;
            n += write(&"Sexpr::Atom(", file);
            n += debug(&as_str(*string), file);
            n += write(&")", file);
            return n;
        }
        Sexpr::Str(string) => {
            let n = 0;
            n += write(&"Sexpr::Str(", file);
            n += debug(&as_str(*string), file);
            n += write(&")", file);
            return n;
        }
        Sexpr::Pair(lt, rt) => {
            let n = 0;
            n += write(&"Sexpr::Pair(", file);
            n += debug(lt.ptr, file);
            n += write(&", ", file);
            n += debug(rt.ptr, file);
            n += write(&")", file);
            return n;
        }
        Sexpr::Error(msg) => {
            let n = 0;
            n += write(&"Sexpr::Error(", file);
            n += debug(&as_str(*msg), file);
            n += write(&")", file);
            return n;
        }
        else => return write(&"...", file);
    }
}
